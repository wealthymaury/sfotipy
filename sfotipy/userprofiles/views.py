from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import View, TemplateView, RedirectView, FormView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .mixins import LoginRequiredMixin
from .forms import UserCreationEmailForm, EmailAuthenticationForm, LoginForm

def singup(request):
	# formulario vacio para el render o lleno cuando ya se hizo la peticion
	form = UserCreationEmailForm(request.POST or None)

	# si el formulario es valido quiero que genere un usuario
	# la validacion ya esta escrita en el Formulario
	if form.is_valid():
		form.save()

		# si se crea el usuario hay que loguearlo de una vez
		# crear el userprofile
		# redireccionarlo al home

	return render(request, 'singup.html', {'form': form})

def signin(request):
	form = EmailAuthenticationForm(request.POST or None)

	if form.is_valid():
		login(request, form.get_user())

		# redireccionalo al home

	return render(request, 'signin.html', {'form': form})

class LoginView(View):
	def get(self, request, *args, **kwargs):
		return HttpResponse('LoginView!!')

# solo vista que hace render
class LoginTemplateView(TemplateView):
	template_name = 'login.html'
	# los kwargs son todos los paramentros que me llegan de la url
	def get_context_data(self, **kwargs):
		# obtengo el context original del padre, para despues yo agregarle valores
		context = super(LoginTemplateView, self).get_context_data(**kwargs)
		# mis variables extra
		is_auth = False
		name = None

		if self.request.user.is_authenticated():
			is_auth = True
			name = self.request.user.username

		data = {
			'is_auth': is_auth, 
			'username': name
		}

		#actualizando el contexto
		context.update(data)

		return context

class LoginFormView(FormView):
	# form_class = LoginForm
	form_class = AuthenticationForm
	template_name = 'loginWithForm.html'
	success_url = '/profile/'

	# este metodo se llama automaticamente si pasan las validaciones de formulario
	# asi que para meter logica adicional hay que sobreescribirlo
	def form_valid(self, form):
		# aqui va tu logica extra

		# # los datos sacalos del diccionario de datos limpios
		# username = form.cleaned_data['username']
		# password = form.cleaned_data['password']

		# # busco al usuario, authenticate me lo devuelve en caso de encontrarlo, si no devuelve false
		# user = authenticate(username=username, password=password)
		# logueo al usuario
		# login(self.request, user)

		# usando AuthenticationForm
		login(self.request, form.get_user())

		return super(LoginFormView, self).form_valid(form)
	
	def get_context_data(self, **kwargs):
		
		context = super(LoginFormView, self).get_context_data(**kwargs)
		
		is_auth = False
		name = None

		if self.request.user.is_authenticated():
			is_auth = True
			name = self.request.user.username

		data = {
			'is_auth': is_auth, 
			'username': name
		}

		context.update(data)

		return context


class ProfileView(LoginRequiredMixin, TemplateView):
	template_name = 'profile.html'

	# @method_decorator(login_required(login_url='/loginform/'))
	# def dispatch(self, request, *args, **kwargs):
	# 	return super(ProfileView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(TemplateView, self).get_context_data(**kwargs)

		if self.request.user.is_authenticated():
			context.update({'userprofile': self.get_user_profile()})

		return context

	def get_user_profile(self):
		return self.request.user.userprofile

class ProfileRedirectView(RedirectView):
	pattern_name = 'profile'
