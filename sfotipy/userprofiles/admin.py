from django.contrib import admin
from .models import Userprofile

class UserprofileAdmin(admin.ModelAdmin):
    list_display = ('avatar', )

admin.site.register(Userprofile, UserprofileAdmin)
