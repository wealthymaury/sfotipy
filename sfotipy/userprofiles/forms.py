from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# ya tiene las cosas del Formulario de creacion de usuario normal
class UserCreationEmailForm(UserCreationForm):
	# Uso los campos del formulario para que ya me valide
	email = forms.EmailField()

	class Meta:
		model = User
		fields = ('username', 'email',)

	# validar que el Email no se repita
	def clean_email(self):
		pass

class EmailAuthenticationForm(forms.Form):
	email = forms.EmailField()
	# en los atributos del campo describo como quiero que sea
	# label es la etiqueta que sale, y widget es input type
	password = forms.CharField(label="Password", widget=forms.PasswordInput)

	def __init__(self, *args, **kwargs):
		self.user_cache = None
		# llamamos al contructor del padra para que haga lo que tiene que hacer y nosotros tambien hacerlo
		super(EmailAuthenticationForm, self).__init__(*args, **kwargs)

	# validaciones
	def clean(self):
		email = self.cleaned_data.get('email')
		password = self.cleaned_data.get('password')

		# traigo al usuario
		self.user_cache = authenticate(email=email, password=password)

		# intentar comprovar que el usuario exista
		if self.user_cache is None:
			# levanto un error
			raise forms.ValidationError('Usuario incorrecto')
		# reviso si el usuario esta activo
		elif not self.user_cache.is_active:
			raise forms.ValidationError('Usuario inactivo')
		
		return self.cleaned_data

	def get_user(self):
		return self.user_cache

class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)
