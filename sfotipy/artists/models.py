from django.db import models

class Artist(models.Model):
	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255, blank=True)
	biography = models.TextField(blank=True)
	# relacion de muchos a muchos con los tracks
	# como ya hay una relacion entre canciones y artistas, para que django no se confunda
	# tenemos que agregar el nombre que se pondria al campo, con related_name
	favorite_songs = models.ManyToManyField('tracks.Track', blank=True, related_name="is_favorite_of")

	def __unicode__(self):
		return "%s - %s" % (self.first_name, self.last_name)
		
