from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.http import JsonResponse

from userprofiles.mixins import LoginRequiredMixin
from albums.models import Album
from tracks.models import Track

# creando un mixin para no repetir codigo
# las clases que hereden de esto tendran disponible este codigo
class JsonResponseMixin(object):

	def response_handler(self):
		# si llega una variable llamada ?format=json debo regresar un json
		# otra opcion podria ser -> if request.is_ajax()
		# si viene format la guardo, si no guardo None
		format = self.request.GET.get('format', None)
		if format == 'json':
			return self.json_to_response()

		# si no entra por el JSON, devo devolver el render normal, asi que lo copeo del padre
		context = self.get_context_data()
		return self.render_to_response(context)

	def json_to_response(self):
		data = self.get_data()
		return JsonResponse(data, safe=False)

# sacar los albums de un artist
# si heredas de un mixin debe ir a la izquierda!!!
class AlbumListView(JsonResponseMixin, ListView):
	# hay que especificar de donde queremos sacar los datos
	model = Album
	# a donde los vamos a mandar
	# al template llega un atributo llamado object_list que es el que debemos recorrer
	template_name = 'album_list.html'
	# paginacion
	paginate_by = 1

	# sobreescribo el metodo get para poder dar respuesta json o render normal
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		return self.response_handler()

	def get_data(self):
		data = list() # tambien puede ser asi []

		# esta es la forma tradicional de lenar una lista
		# for album in self.object_list:
		# 	data.append({
		# 		'cover': album.cover.url,
		# 		'title': album.title,
		# 		'artist': album.artist.first_name,
		# 	})

		# python trae una forma mas chida de llenar la lista con algo que se llama comprehension
		data = [{
			'cover': album.cover.url,
			'title': album.title,
			'artist': album.artist.first_name,
		} for album in self.object_list]

		return data

	# si queremos definir el queryset es asi, sobreescribiendo el metodo
	def get_queryset(self):
		# como hay 2 urls que apuntan aqui mismo, debo ver si llega el parametro o no que especifica el artista
		# los parametros de URL llegan a kwargs que es un diccionario que tiene las variables de la URL
		# si existe el parametro artista
		if self.kwargs.get('artist'):
			# devolver albums de artista, observa la condicion como parametro
			# observa el __contains que es como un like :D hay muchas cosas mas que se pueden hacer
			queryset = self.model.objects.filter(artist__first_name__contains=self.kwargs['artist'])
		else:
			# que actue como normalmente lo haria, llamando al padre
			queryset = super(AlbumListView, self).get_queryset()

		return queryset

class AlbumDetailView(LoginRequiredMixin, JsonResponseMixin, DetailView):
	model = Album
	template_name = 'album_detail.html'

	# de igual manera, para devolver json aqui hay que sobreescribir el metodo
	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		return self.response_handler()

	def get_data(self):
		data = {
			'album' : {
				'cover': self.object.cover.url,
				'title': self.object.title,
				'artist': self.object.artist.first_name,
				'tracks': [t.title for t in self.object.track_set.all()]
			}
		}

		return data

# usando un queryset personalizado
class TopTrackListView(ListView):
	# top es una funcion que yo hice, ver tracks.models
	# queryset = Track.objects.top()
	queryset = Track.objects.top().filter(artist__pk=1)[:2]
	template_name = 'track_list.html'

