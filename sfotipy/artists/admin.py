from django.contrib import admin

from .models import Artist
from tracks.models import Track
from albums.models import Album

# para que desde la interfaz de editar artista pueda yo editar sus canciones
class TrackInline(admin.StackedInline):
	model = Track
	# campos vacios para poder crear nuevos tracks desde la interfaz de editar artsita
	extra = 1

class AlbumInline(admin.StackedInline):
	model = Album
	extra = 1

class ArtistAdmin(admin.ModelAdmin):
	search_fields = ('first_name', 'last_name',)
	# me pone una interfaz mas usable para relacion de muchos a muchos,
	# cuando los nombres de los elementos son muy largos, usa filter_vertical
	filter_horizontal = ('favorite_songs',)
	# usando las clases de attiba
	inlines = [TrackInline, AlbumInline]

admin.site.register(Artist, ArtistAdmin)
