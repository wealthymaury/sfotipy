from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView, RedirectView

from userprofiles.views import LoginView, LoginTemplateView, ProfileView, ProfileRedirectView, LoginFormView
from artists.views import AlbumListView, AlbumDetailView, TopTrackListView

urlpatterns = [
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tracks/(?P<title>[\w\-\W]+)/', 'tracks.views.track_view', name="track_view"),
    url(r'^singup/', 'userprofiles.views.singup', name="singup"),
    url(r'^signin/', 'userprofiles.views.signin', name="signin"),
    url(r'^login/', LoginView.as_view(), name="login"),
    url(r'^logint/', TemplateView.as_view(template_name='login.html'), name="logint"),
    url(r'^logint2/', LoginTemplateView.as_view(), name="logint2"),
    url(r'^profile/', ProfileView.as_view(), name="profile"),
    url(r'^perfil/', RedirectView.as_view(pattern_name='profile'), name="perfil"),
    url(r'^perfil2/', ProfileRedirectView.as_view(), name="perfil2"),
    # recuperar password
    url(r'^admin/password_reset/$', 'django.contrib.auth.views.password_reset', name='admin_password_reset'),
    url(r'^admin/password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    # class based views
    url(r'^albums/$', AlbumListView.as_view(), name='album_list'),
    url(r'^albums/(?P<artist>[\w\-]+)/$', AlbumListView.as_view(), name='album_list'),
    url(r'^albums/detail/(?P<pk>[\w\-]+)/$', AlbumDetailView.as_view(), name='album_detail'),
    url(r'^loginform/$', LoginFormView.as_view(), name='loginformview'),
    url(r'^track/top/$', TopTrackListView.as_view(), name='tracks_top'),
]

if settings.DEBUG:
	urlpatterns += [
		url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	]
