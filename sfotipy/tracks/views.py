import json

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import Track

def track_view(request, title):
	track = get_object_or_404(Track, title=title)

	# asi se hace debug, este es como un punto de interrupcion
	# import ipdb; ipdb.set_trace()

	# respuesta JSON
	data = {
		'title': track.title,
		'order': track.order,
		'album': track.album.title,
		'artist': {
			'name': track.artist.first_name,
			'biography': track.artist.biography
		}
	}

	# json_data = json.dumps(data)
	# return HttpResponse(json_data, content_type='application/json')

	# si lo que quiero es recibir un JSON y convertirlo en un diccionario de python lo que hago es
	# data = json.loads(json_data)

	# render normal
	return render(request, 'track.html', {'track' : track})
