from django.db import models
from django.db.models import QuerySet

from albums.models import Album
from artists.models import Artist

class TrackQuerySet(QuerySet):
	def top(self):
		return self.order_by('-order')

class Track(models.Model):
	title = models.CharField(max_length=255)
	order = models.PositiveIntegerField()
	track_file = models.FileField(upload_to='tracks')
	album = models.ForeignKey(Album)
	artist = models.ForeignKey(Artist)

	# redefiniendo el manager de consultas
	# aparte de all, filter, etc.. tengo disponible top :D
	objects = TrackQuerySet.as_manager()

	# def player(self):
	# 	return self.track_file.url

	# esta funcion se usa desde el administrador
	def player(self):
		return """
			<audio controls>
				<source src="%s" type="audio/mpeg">
				Your browser does not support the audio tag.
			</audio>
		""" % self.track_file.url

	# como las funciones tambien son objetos, puedes configurarle atributos
	# para este caso es para que permita que la cadena se interprete como HTML
	player.allow_tags = True
	# para que permita que el campo de funcion se ordene en el administrador
	player.admin_order_field = 'track_file'

	# con esto ganamos funciones
	# en el administrador, cuando me valla a editar un elemento, django
	# pondra un boton de ir a ver en el sitio, que mandaria al resultado de
	# esta url
	def get_absolute_url(self):
		return '/tracks/' + self.title

	def __unicode__(self):
		return self.title