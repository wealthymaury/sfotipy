from django.contrib import admin

from .models import Track
from actions import export_as_excel

@admin.register(Track)
class TrackAdmin(admin.ModelAdmin):
	list_display = ('title', 'artist', 'order', 'album', 'player', 'es_uno')
	list_filter = ('artist', 'album',)
	search_fields = ('title', 'artist__first_name', 'artist__last_name',)
	list_editable = ('album', 'order')
	actions = (export_as_excel,)
	raw_id_fields = ('artist',)

	def es_uno(self, obj):
		return obj.id == 1

	# le pone el iconito de palomita o tache
	es_uno.boolean = True

# ya no se usa, ahora es mejor el decorator :D
# admin.site.register(Track, TrackAdmin)